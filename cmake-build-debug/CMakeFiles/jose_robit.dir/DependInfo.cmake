# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/valeness/CLionProjects/jose_robit/src/main.cpp" "/home/valeness/CLionProjects/jose_robit/cmake-build-debug/CMakeFiles/jose_robit.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/valeness/.platformio/packages/framework-arduinoavr/cores/arduino"
  "/home/valeness/.platformio/packages/framework-arduinoavr/variants/standard"
  "../src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/__cores__/arduino/EEPROM/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/__cores__/arduino/HID/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/__cores__/arduino/SPI/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/__cores__/arduino/SoftwareSerial/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/__cores__/arduino/Wire/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Adafruit_CircuitPlayground"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Adafruit_CircuitPlayground/utility"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Bridge/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Esplora/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Ethernet/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Firmata"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Firmata/utility"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/GSM/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Keyboard/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/LiquidCrystal/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Mouse/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/RobotIRremote/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Robot_Control/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Robot_Motor/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/SD/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Servo/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/SpacebrewYun/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Stepper/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/TFT/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/Temboo/src"
  "/home/valeness/.platformio/packages/framework-arduinoavr/libraries/WiFi/src"
  "/home/valeness/.platformio/packages/toolchain-atmelavr/avr/include"
  "/home/valeness/.platformio/packages/toolchain-atmelavr/lib/gcc/avr/4.9.2/include-fixed"
  "/home/valeness/.platformio/packages/toolchain-atmelavr/lib/gcc/avr/4.9.2/include"
  "/home/valeness/.platformio/packages/tool-unity"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
